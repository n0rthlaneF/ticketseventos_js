package br.pucpr.bsi.prog3.ticketsEventosBSI.exception;


/**
 * Classe de Exception para o projeto de Tickets Eventos
 * @author Mauda
 *
 */

public class BSIException extends RuntimeException{

	private static final long serialVersionUID = 4928599035264976611L;
	
	public BSIException(String message) {
		super(message);
	}
	
}
