package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.util;

import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;

/**
 * Classe responsavel por atualizar informacoes para update e comparacoes
 * @author Mauda
 *
 */
public class Atualizador {
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param objeto
	 * @param cod
	 */
	public static void atualizar(Artista objeto, String cod){
		objeto.setNome(cod 		+ objeto.getNome());
	}
	
}
